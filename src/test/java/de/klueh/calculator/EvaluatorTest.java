package de.klueh.calculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class EvaluatorTest {

    private final Evaluator evaluator = new Evaluator();

    @ParameterizedTest
    @ValueSource(strings = {"0", "1","-1","42","-1337","-1,337",""})
    void onlyNumber(final String value) {
        assertExpressionIsEqualTo(value, value);
    }

    @Test
    void simpleAddition0(){
        assertExpressionIsEqualTo("0#+#0","0");
    }

    @Test
    void simpleAddition1(){
        assertExpressionIsEqualTo("1#+#0", "1");
    }

    @Test
    void simpleAddition2(){
        assertExpressionIsEqualTo("-1#+#0","-1");
    }

    @Test
    void simpleAddition3(){
        assertExpressionIsEqualTo("-1#+#-42","-43");
    }

    @Test
    void simpleAddition4(){
        assertExpressionIsEqualTo("5#+#-42","-37");
    }

    @Test
    void additionAndMultiplication0(){
        assertExpressionIsEqualTo("5#+#-2#*#6","-7");
    }

    @Test
    void additionAndMultiplication1(){
        assertExpressionIsEqualTo("5#+#-2#*#6#+#7#*#0#+#8#*#0.5","-3.0");
    }

    @Test
    void subtractionAndMultiplication0(){
        assertExpressionIsEqualTo("5#-#-2#*#6","17");
    }

    @Test
    void subtractionAndMultiplication1(){
        assertExpressionIsEqualTo("5#-#-2#*#6#-#7#*#0#+#8#*#0.5","21.0");
    }

    @Test
    void additionAndDivision0(){
        assertExpressionIsEqualTo("5#+#6#/#-3","3");
    }

    @Test
    void additionAndDivision1(){
        assertExpressionIsEqualTo("5#+#-2#/#6#+#7#/#0#+#8#/#0.5","errorDivisionByZero");
    }

    @Test
    void subtractionAndDivision0(){
        assertExpressionIsEqualTo("5#-#6#/#-2","8");
    }

    @Test
    void subtractionAndDivision1(){
        assertExpressionIsEqualTo("5#-#-2#/#6#-#7#/#0#+#8#/#0.5","errorDivisionByZero");
    }

    private void assertExpressionIsEqualTo(final String expression, final String result){
        final String actual = evaluator.evaluateExpression(expression);

        assertThat(actual).isEqualTo(result);
    }
}