package de.klueh.calculator;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static de.klueh.calculator.Evaluator.SEPARATOR;

@Controller
@RequiredArgsConstructor
public class MainController implements Initializable {

    private final StringProperty number = new SimpleStringProperty("0");
    private final BooleanProperty negative = new SimpleBooleanProperty(false);
    private final Evaluator evaluator;
    private final StringBuilder expression = new StringBuilder();
    @FXML
    private Parent root;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    @FXML
    private Button btn5;
    @FXML
    private Button btn6;
    @FXML
    private Button btn7;
    @FXML
    private Button btn8;
    @FXML
    private Button btn9;
    @FXML
    private Button btn0;
    @FXML
    private Button btnEquals;
    @FXML
    private Button btnDot;
    @FXML
    private Button btnPlus;
    @FXML
    private Button btnMinus;
    @FXML
    private Button btnMultiply;
    @FXML
    private Button btnDivide;
    @FXML
    private Button btnC;
    @FXML
    private Button btnCE;
    @FXML
    private Button btnPercent;
    @FXML
    private Button btnSign;
    @FXML
    private TextField textField;
    @FXML
    private Label lblExpression;
    @FXML
    private HBox titleBar;

    private boolean fragmented;

    private double xOffset = 0.;
    private double yOffset = 0.;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        final List<Button> numberButtons = List.of(btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9);
        numberButtons.forEach(btn -> btn.setOnAction(event -> addNumber(((Button) event.getSource()).getText())));

        btnSign.setOnAction(event -> negative.setValue(!negative.getValue()));
        btnDot.setOnAction(event -> fragmentNumber());

        number.addListener((observable, oldValue, newValue) -> {
            if (newValue.matches("0[0-9]")) {
                number.setValue(newValue.substring(1, 2));
            }
            textField.setText(userFriendlyNumber());
        });
        negative.addListener((observable, oldValue, newValue) -> textField.setText(userFriendlyNumber()));

        root.sceneProperty().addListener((observable, oldValue, newValue) -> newValue.setOnKeyReleased(event -> {
            if (event.getCode().isDigitKey()) {
                addNumber(event.getText());
            } else if (event.getText().matches("[,.]")) {
                fragmentNumber();
            }
        }));

        root.sceneProperty().addListener((observable, oldValue, newValue) -> newValue.setOnKeyTyped(event -> {
            if (event.getCharacter().matches("[+\\-*/]")) {
                handleOperation(event.getCharacter());
            }
        }));

        final List<Button> operators = List.of(this.btnPlus, btnMinus, btnMultiply, btnDivide);
        operators.forEach(btn -> btn.setOnAction(event -> handleOperation(((Button) event.getSource()).getText())));

        btnCE.setOnAction(event -> clear());
        btnC.setOnAction(event -> {
            expression.setLength(0);
            lblExpression.setText("");
            clear();
        });

        btnEquals.setOnAction(event -> {
            if (negative.getValue()) {
                expression.append("-");
            }
            expression.append(number.getValue());
            number.setValue(evaluator.evaluateExpression(expression.toString()));
            lblExpression.setText(expression.toString());
            expression.setLength(0);
        });

        //noinspection BigDecimalMethodWithoutRoundingCalled
        btnPercent.setOnAction(event -> number.setValue(new BigDecimal(number.getValue()).divide(new BigDecimal(100)).toString()));

        initDraggability(titleBar);
    }

    private void handleOperation(final String operation) {
        if (negative.getValue()) {
            expression.append("-");
        }
        expression.append(number.getValue());
        expression.append(SEPARATOR);
        expression.append(operation);
        lblExpression.setText(expression.toString());
        expression.append(SEPARATOR);
        clear();
    }

    private void clear() {
        number.setValue("0");
        fragmented = false;
        negative.setValue(false);
    }


    private void addNumber(final String text) {
        number.setValue(number.getValue() + text);
    }

    private void fragmentNumber() {
        if (!fragmented) {
            addNumber(".");
            fragmented = true;
        }
    }

    private String userFriendlyNumber() {
        final String[] split = number.getValue().split("\\.");

        final StringBuilder numberBuilder = new StringBuilder(split[0]);

        if (split[0].length() > 3 && split[0].matches("[0-9]+")) {
            int remaining = 0;
            while ((remaining += 3) < split[0].length()) {
                numberBuilder.insert(split[0].length() - remaining, ".");
            }
        }

        return (negative.getValue() ? "-" : "") + numberBuilder.toString() + ((split.length > 1) ? "," + split[1] : "");
    }

    @FXML
    @SuppressWarnings({"RedundantSuppression", "unused"})
    private void close(final ActionEvent event) {
        Platform.exit();
    }

    private void initDraggability(final Node source) {
        source.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });
        source.setOnMouseDragged(event -> {
            source.getScene().getWindow().setX(event.getScreenX() - xOffset);
            source.getScene().getWindow().setY(event.getScreenY() - yOffset);
        });
    }
}
