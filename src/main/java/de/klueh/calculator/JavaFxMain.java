package de.klueh.calculator;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class JavaFxMain extends Application {
    private ConfigurableApplicationContext applicationContext;

    @Override
    public void init() throws Exception {
        super.init();
        final ApplicationContextInitializer<GenericApplicationContext> initializer = ac -> {
            ac.registerBean(Application.class, () -> JavaFxMain.this);
            ac.registerBean(Parameters.class, this::getParameters);
            ac.registerBean(HostServices.class, this::getHostServices);
        };
        applicationContext = new SpringApplicationBuilder()
                .sources(CalculatorApplication.class)
                .initializers(initializer)
                .run(getParameters().getRaw().toArray(String[]::new));
    }

    @Override
    public void start(final Stage primaryStage) {
        applicationContext.publishEvent(new StageReadyEvent());
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        applicationContext.close();
    }
}

class StageReadyEvent extends ApplicationEvent {
    StageReadyEvent() {
        super(new Stage());
    }

    public Stage getStage() {
        return (Stage) source;
    }
}
