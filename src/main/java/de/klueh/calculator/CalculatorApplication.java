package de.klueh.calculator;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculatorApplication {

    public static void main(String[] args) {
        Application.launch(JavaFxMain.class, args);
    }

}
