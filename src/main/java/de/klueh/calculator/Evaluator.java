package de.klueh.calculator;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Evaluator {

    public static final String SEPARATOR = " ";

    public String evaluateExpression(final String expression) {

        final String[] split = expression.split(SEPARATOR);

        try {
        return evaluate(Arrays.stream(split).collect(Collectors.toList()));
        }catch (final ArithmeticException exception){
            return "errorDivisionByZero";
        }
    }

    private String evaluate(final List<String> split) {
        if (split.size() == 1) {
            return split.get(0);
        }

        final int firstProduct = split.indexOf("*");
        final int firstDivision = split.indexOf("/");
        if (firstProduct != -1) {
            final String multiplicand = split.remove(firstProduct + 1);
            split.remove(firstProduct);
            split.set(firstProduct - 1, multiply(split.get(firstProduct - 1), multiplicand));
            return evaluate(split);
        }else if(firstDivision !=-1){
            final String divisor = split.remove(firstDivision + 1);
            split.remove(firstDivision);
            split.set(firstDivision - 1, divide(split.get(firstDivision - 1), divisor));
            return evaluate(split);
        } else {
            if (split.get(1).equals("+")) {
                final String sum = new BigDecimal(split.get(0)).add(new BigDecimal(split.get(2))).toString();

                final List<String> rest = split.subList(3, split.size());
                rest.add(0, sum);

                return evaluate(rest);
            } else if (split.get(1).equals("-")) {
                final String sum = new BigDecimal(split.get(0)).subtract(new BigDecimal(split.get(2))).toString();

                final List<String> rest = split.subList(3, split.size());
                rest.add(0, sum);

                return evaluate(rest);
            } else {
                return "unsupported operation";
            }
        }
    }

    private String divide(final String dividend, final String divisor) {
        return new BigDecimal(dividend).divide(new BigDecimal(divisor), RoundingMode.HALF_UP).toString();
    }

    private String multiply(final String multiplier, final String multiplicand) {
        return new BigDecimal(multiplier).multiply(new BigDecimal(multiplicand)).toString();
    }


}
